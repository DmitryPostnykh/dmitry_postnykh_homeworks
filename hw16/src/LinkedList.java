public class LinkedList<T> {

    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private int size;

    public void add(T element) {
        Node<T> newNode = new Node<>(element);
        if (size == 0) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }
    public void addToBegin(T element) {
        Node<T> newNode = new Node<>(element);
        if (size == 0) {
            last = newNode;
        } else {
            newNode.next = first;
        }
        first = newNode;
        size++;
    }
    public int size() {
        return size;
    }
    public T get(int i) {
        if (isCorrectIndex(i)) {
            Node<T> currentNode = first;

            for (int cnt = 1; cnt <= i; cnt++) {
                currentNode = currentNode.next;
            }
            return currentNode.value;
        } else {
            return null;
        }
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

}
