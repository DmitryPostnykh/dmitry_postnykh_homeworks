
public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(23);
        arrayList.add(33);
        arrayList.add(-45);
        arrayList.add(4);
        arrayList.add(654);
        arrayList.add(-566);
        arrayList.add(-55);
        arrayList.add(6);
        arrayList.add(-768);
        arrayList.add(9875);

        arrayList.deleteIndex(2);

        String res1 = "";
        for (int index = 0; index < arrayList.getSize(); index++) {
            res1 += " " + arrayList.get(index);
        }
        System.out.println(res1);
        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.add("три");
        linkedList.add("один");
        linkedList.add("одиннадцать");
        linkedList.add("три");
        linkedList.add("девять");
        linkedList.add("три");
        linkedList.add("три");
        linkedList.add("один");
        linkedList.add("один");
        linkedList.add("восемь");
        linkedList.add("одиннадцать");
        linkedList.add("десять");

        String res2 = linkedList.get(6);
        System.out.println(res2);
    }
}
