import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = {13, 22, 23, -4, 65, -68, -1, 44, -765, 239};

        ByCondition cond1 = number -> (number % 2 == 0);
        ByCondition cond2 = number -> {

            number = Math.abs(number);

            int summ = 0;
            while (number > 0) {
                summ += number % 10;
                number = number / 10;
            }
            return summ % 2 == 0;
        };

        int[] usl1 = Sequence.filter(array, cond1);
        int[] usl2 = Sequence.filter(array, cond2);

        System.out.println("Условие 1: " + Arrays.toString(usl1));
        System.out.println("Условие 2: " + Arrays.toString(usl2));
    }
}
