import java.util.ArrayList;

import java.util.List;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition) {

        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {

            if (condition.isOk(array[i])) {
                result.add(array[i]);
            }
        }
        int[] resultArray = result.stream().mapToInt(i->i).toArray();
        return resultArray;
    }
}
