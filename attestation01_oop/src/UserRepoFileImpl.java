import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepoFileImpl implements UsersRepository {

    private String fileName;

    public UserRepoFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (Reader reader = new FileReader(fileName); BufferedReader bufferedReader = new BufferedReader(reader)) {
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                int age = Integer.parseInt(parts[2]);
                boolean isWorker = Boolean.parseBoolean(parts[3]);
                User newUser = new User(id, name, age, isWorker);
                users.add(newUser);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        return users;
    }

    @Override
    public void save(User user) {
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            bufferedWriter.write(user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public User findById(int id) {
        User user = null;
        try (Reader reader = new FileReader(fileName); BufferedReader bufferedReader = new BufferedReader(reader)) {
            String line = bufferedReader.readLine();
            int parsedId = -1;
            while (line != null && parsedId != id) {
                String[] parts = line.split("\\|");
                parsedId = Integer.parseInt(parts[0]);
                if (parsedId == id) {
                    String name = parts[1];
                    int age = Integer.parseInt(parts[2]);
                    boolean isWorker = Boolean.parseBoolean(parts[3]);
                    user = new User(id, name, age, isWorker);
                }
                line = bufferedReader.readLine();
            }
            if (user != null) {
                user.setName("Марсель");
                user.setAge(27);
                update(user);
            }
            return user;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User user) {
        StringBuilder inputBuffer = new StringBuilder();
        try (Reader reader = new FileReader(fileName); BufferedReader bufferedReader = new BufferedReader(reader)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                if (id != user.getId()) {
                    inputBuffer.append(line);
                    inputBuffer.append('\n');
                } else {
                    inputBuffer.append(user.getId()).append("|").append(user.getName()).append("|")
                            .append(user.getAge()).append("|").append(user.isWorker());
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        try (FileOutputStream fileOut = new FileOutputStream(fileName)) {
            fileOut.write(inputBuffer.toString().getBytes());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
