import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");

        List<User> users1 = usersRepository.findByAge(25);

        System.out.println("result:");
        for (User user : users1) {
            System.out.println(user.toString());
        }

       /* List<User> users2 = usersRepository.findByIsWorkerIsTrue();

        System.out.println("\n" + "result:");
        for (User user : users2) {
            System.out.println(user.toString());*/
        }
    }
}
