public class SortDouble {

    public static void doubbleSort(Human[] array) {
        boolean sorted = false;
        Human temp;
        while (!sorted) {
            sorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i].getWeight() > array[i + 1].getWeight()) {
                    temp = new Human(array[i].getName(), array[i].getWeight());
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                    sorted = false;
                }
            }
        }
    }
}
