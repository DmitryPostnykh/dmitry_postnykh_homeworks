public class Human {
    private String name;
    private Double weight;

    public Human(String name, Double weight) {
        this.name = name;
        if (weight < 200) {                 //максимальный вес 200 кг
            this.weight = weight;
        } else {
            this.weight = 200.0;
        }
    }

    public String getName() {

        return this.name;
    }

    public Double getWeight() {

        return this.weight;
    }

    public String toString() {

        return ("Имя: " + this.name + " \n" + "Вес: " + this.weight + " кг.\n");
    }
}
