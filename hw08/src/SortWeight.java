import java.io.BufferedReader;
import java.io.InputStreamReader;

public class SortWeight {

    public static void weightSort() throws Exception {
        Human[] humanArray = new Human[10];
        System.out.println("Введите 10 раз имя и вес: ");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < 10; i++) {
            String inputString = reader.readLine();
            String[] namePlusWeight = inputString.split(" ");
            humanArray[i] = new Human(namePlusWeight[0], Double.parseDouble(namePlusWeight[1]));
        }

        SortDouble.doubbleSort(humanArray);

        for (Human human : humanArray) {
            System.out.println(human.toString());
        }
    }}
