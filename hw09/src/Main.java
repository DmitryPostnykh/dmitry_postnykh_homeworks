public class Main {

    public static void main(String[] args) throws Exception {

        Ellipse ellipse = new Ellipse(10, 23, 86, 55);
        Circle circle = new Circle(3, 5, 10);
        Rectangle rectangle = new Rectangle(4, 4, 5, 10);
        Square square = new Square(5, -40, 7);
        Figure figure = new Figure(5, 7);

        System.out.println("Периметр фигуры = " + figure.getPerimeter());
        System.out.println("Периметр эллипса = " + ellipse.getPerimeter());
        System.out.println("Периметр круга = " + circle.getPerimeter());
        System.out.println("Периметр прямоугольника = " + rectangle.getPerimeter());
        System.out.println("Периметр квадрата = " + square.getPerimeter());
    }
    public static class InitializeFigureException extends Exception {

        public InitializeFigureException(String message) {
            super(message);
        }
    }
}

