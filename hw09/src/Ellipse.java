
public class Ellipse extends Figure {

    protected int radA;
    protected int radB;

    public Ellipse(int x, int y, int radiusA, int radiusB) throws Exception {
        super(x, y);
        if ((radiusB <= 0) || (radiusA <= 0)) {
            throw new Main.InitializeFigureException("radius must be > 0, current input values: " + "R1 = " + radiusA + " R2 = " + radiusB);
        } else {
            this.radA = radiusA;
            this.radB = radiusB;
        }
    }

    @Override
    public Double getPerimeter() {
        int radiusBig;
        int radiusSmall;

        if (radA >= radB) {
            radiusBig = radA;
            radiusSmall = radB;
        } else {
            radiusBig = radB;
            radiusSmall = radA;
        }

        return 4 * (Math.PI * radiusBig * radiusSmall + Math.pow((radiusBig - radiusSmall), 2)) / (radiusBig + radiusSmall);
    }
}

