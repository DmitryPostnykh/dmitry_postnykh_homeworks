public class Square extends Rectangle{

    public Square(int x, int y, int side) throws Exception{

        super(x, y, side, side);

    }

    @Override
    public Double getPerimeter() {

        return 4.0 * this.height;
    }
}

