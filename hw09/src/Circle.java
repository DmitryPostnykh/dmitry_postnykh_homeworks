public class Circle extends Ellipse {

    public Circle(int x, int y, int radius) throws Exception {

        super(x, y, radius, radius);
    }

    @Override
    public Double getPerimeter() {

        return 2 * Math.PI * this.radA;
    }

}
