
public class Rectangle extends Figure {

    protected int height;
    protected int width;

    public Rectangle(int x, int y, int height, int width) throws Exception {
        super(x, y);

        if ((height <= 0) || (width <= 0)) {
            throw new Main.InitializeFigureException("side must be > 0, current input values: " + "height = " + height + " width = " + width);
        } else {
            this.height = height;
            this.width = width;
        }
    }

    @Override
    public Double getPerimeter() {
        return this.height * 2.0 + this.width * 2;
    }
}

