import java.util.*;

public class hw07 {

   public static int[] selectArr(int[] array) {

        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    minIndex = j;
                }
            }
            int temp = array[i];
            array[i] = array[minIndex];
            array[minIndex] = temp;
        }
        return array;
    }

    public static int[] findCount(int[] arr) {
        int[] result = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arr[i] == arr[j])
                    result[i]++;
            }
        }
        return result;
    }

    public static Set<Integer> minDigit(int[] arrFirst, int[] arrCount) {
        Set<Integer> treeSet = new TreeSet<>();
        int indexMin = 0;
        int min = arrCount[0];
        for (int i = 0; i < arrFirst.length; i++) {
            for (int j = i; j < arrCount.length; j++) {
                if (arrCount[j] <= min) {
                    min = arrCount[j];
                    indexMin = j;
                    treeSet.add(arrFirst[indexMin]);
                }
            }
        }
        return treeSet;
    }
    public static int[] getArray() {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        ArrayList<Integer> arr = new ArrayList<>();

        while (a != -1) {
            if (a < -100 || a > 100) {
                System.out.println("Нарушен диапазон от -100 до 100 !!!");
                break;
            }
            arr.add(a);
            a = scanner.nextInt();
        }
        int sizeArr = arr.size();
        Integer[] array = arr.toArray(new Integer[sizeArr]);
        int[] result = Arrays.stream(array).mapToInt(i -> i).toArray();

        return result;
    }

    public static void main(String[] args) {

        System.out.print("Заполняем массив числами: \n");
        int[] arrFirst = selectArr(getArray());
        int[] arrCount = findCount(arrFirst);
        System.out.println(Arrays.toString(arrFirst));
        System.out.println(Arrays.toString(arrCount));
        System.out.println("Минимально раз: " + minDigit(arrFirst, arrCount));

    }
}
