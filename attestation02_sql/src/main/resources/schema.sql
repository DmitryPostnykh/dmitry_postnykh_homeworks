create table product
(
    id serial primary key,
    name varchar(30),
    description varchar(50),
    cost integer check (cost > 0),
    count integer check (count > 0)
);

create table customer
(
    id serial primary key,
    first_name varchar(30),
    last_name varchar(30),
    email varchar(20)
);

create table orders (
                        id serial primary key,
                        product_id integer,
                        customer_id integer,
                        order_date date,
                        count_products integer,
                        foreign key(product_id) references product(id),
                        foreign key(customer_id) references customer(id)
);

insert into product(name, description, cost, count) values ('Toyota', 'Business class', 1500000, 1);
insert into product(name, description, cost, count) values ('UAZ 469', 'All road', 11200, 2);
insert into product(name, description, cost, count) values ('KIA RIO', 'Female car', 560000, 5);
insert into product(name, description, cost, count) values ('Hyundai Solaris', 'Standart car', 250000, 10);
insert into product(name, description, cost, count) values ('Lamborghini', 'Super Car', 1250000, 1);

update product set cost = 1600000 where id = 1;

insert into customer(first_name, last_name, email) values ('Алексей', 'Раневский', 'runal@maail.ru');
insert into customer(first_name, last_name, email) values ('Александр', 'Худяков', 'hudal@maail.ru');
insert into customer(first_name, last_name, email) values ('Виктория', 'Сидорова', 'sidvika@gmaail.com');
insert into customer(first_name, last_name, email) values ('Евгений', 'Панин', 'paninzhe@maail.ru');
insert into customer(first_name, last_name, email) values ('Ирина', 'Пронягина', 'irapro@yande.ru');

insert into orders(order_date, customer_id, product_id, count_products) values ('2020-01-23', 1, 1, 1);
insert into orders(order_date, customer_id, product_id, count_products) values ('2019-11-12', 2, 2, 3);
insert into orders(order_date, customer_id, product_id, count_products) values ('2019-05-12', 3, 3, 2);
insert into orders(order_date, customer_id, product_id, count_products) values ('2020-11-24', 4, 4, 10);
insert into orders(order_date, customer_id, product_id, count_products) values ('2017-04-10', 5, 2, 7);
insert into orders(order_date, customer_id, product_id, count_products) values ('2020-06-14', 1, 4, 15);


/* Все товары */
select * from product;

/* Все заказы */
select
    order_date,
    customer.last_name,
    customer.first_name,
    product.name,
    product.cost,
    orders.count_products
from orders, customer, product
where orders.product_id = product.id and orders.customer_id = customer.id;

/* Количество проданных товаров и их остаток */
select a.name as product_name, sum(p.count_products) as sold_count, a.count as in_stock
from product a
         full join orders p on p.product_id = a.id
group by a.id
order by sold_count DESC;

/* Список товоаров, с количеством заказов, в которых он присутствует */
select name, (select count (*) from orders where product_id = product.id) as orders_count from product;

/* Покупатели, которые совершали покупки */
select a.last_name, a.first_name, a.email
from customer a
         left join orders o on a.id = o.customer_id
group by a.id
order by a.last_name;
