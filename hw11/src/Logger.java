
public class Logger {

    private static final Logger logger;

    static {
        logger = new Logger();
    }
    private String[] names = {"Илья", "Алеша", "Добрыня"};

    public static Logger getLogger() {
        return logger;
    }

    public void log(String login) {
        int i = 0;
        for (String x : names) {
            if (x.equalsIgnoreCase(login)) {
                System.out.println("Верно! Есть такой!");
                i = 1;
            }
        }
        if (i == 0) System.err.println("Нет такого в нашем списке!");
    }
}