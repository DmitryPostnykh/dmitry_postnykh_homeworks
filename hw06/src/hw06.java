
public class hw06 {

    public static int getIntex(int[] arr, int num) {    // поиск индекса в массиве
        int index = -1;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == num) {
                index = i;
            }
        }
        return index;
    }

    public static void sorterZero(int[] arr) {          // сортируем нули
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == 0) {
                    arr[i] = arr[j];
                    arr[j] = 0;
                }
            }
            System.out.print(arr[i] + " ");
        }
    }

    public static void main(String[] args) {
        int[] arr = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20}; // массив произвольных чисел
        int number = 14;                                    // искомое число в массиве

        System.out.println("Индекс: " + getIntex(arr, number));

        sorterZero(arr);
    }
}