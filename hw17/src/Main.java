import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        String initial_text = "Например, если система третьей стороны присылает нам уведомления, что заказ отправлен, неплохо было бы сделать лог запросов которые эта система присылает и ответов, которые высылает наша система. ";
        String text = initial_text.replace('.', ' ');

        Map<String, Integer> map = new HashMap<>();
        String[] strArray = text.split(" ");

        for(String s : strArray) {
            if(!map.containsKey(s)) {
                map.put(s, 1);
            }else {
                int value = map.get(s);
                value++;
                map.put(s, value);
            }
        }


        for(String word : map.keySet()) {
            System.out.printf("\nСлово %s встечается %d %s.", word, map.get(word),
                    resolveSuffix(map.get(word)));
        }
    }

    private static String resolveSuffix(int n) {
        String s = String.valueOf(n);
        if(s.endsWith("2") || s.endsWith("3") || s.endsWith("4")) {
            return "раза";
        }else {
            return "раз";
        }
    }

}
