import java.util.Scanner;


public class hw05 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int scan = scanner.nextInt();
        if (scan == -1) {
            System.out.println("Вы ввели число, которое завершает программу по условию задачи");
        } else {
            int minDigit = 10;
            scan = Math.abs(scan);          // вычисляем значение по модулю
            while (scan != -1) {            // пока не ввели число -1 проводим работу над разбиением
                while (scan != 0) {         // если число не равно нулю, разбираем его на цифры
                    int b = scan % 10;      // остаток от деления
                    scan = scan / 10;       // отсекаем один знак
                    if (b < minDigit) {     // выясняем минимум
                        minDigit = b;
                    }
                }
                scan = scanner.nextInt();   // повторяем ввод числа
            }
            int c = (minDigit == 10 ? 0 : minDigit);
            System.out.println(c);   // ВЫВОДИМ ИСКОМОЕ ЗНАЧЕНИЕ
        }
    }
}
